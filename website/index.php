<!DOCTYPE HTML>
<!--
	Escape Velocity by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>The project :: COINS project team 8 - coolhunting about "asylum seekers"</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper" class="wrapper">
					<div id="header">
						<!-- Logo -->
						<!-- Nav -->
							<?php $selected="home"; include ("nav.php"); ?>

					</div>
				</div>

			<!-- Intro -->
				<div id="intro-wrapper" class="wrapper style1">
					<div class="title">Key topic</div>
					<section id="intro" class="container">
						<!-- <p class="style1">Our key question is...</p> -->
						<p class="style2">
							What are the opinions, attitudes and morales concerning refugees in different European countries<br class="mobile-hide" />
							and how do they differ? <a href="http://html5up.net" class="nobr"></a>
						</p>
					</section>
				</div>

			<!-- Main -->
				<div class="wrapper style2" style="padding: 0 0 110px 0;">
					<div class="title">Purpose</div>
					<div id="main" class="container">

						<header class="style1">
						<p>The purpose of the project is to capture opinions and sentiments concerning the so-called

							refugee crises in Europe from Summer 2015 until Summer 2016, as well as to identify the

							key actors in the debate. <br>Hereby, a special focus is put on differences between countries

							and language clusters. <br>Furthermore, the project aims on highlighting and understanding

							connections between sentiments and core events during the refugee crisis.</p>
						</header>

						<!-- Image -->

								<img src="images/child-unhcr-start.jpg" alt="" />
								<p style="margin: 0; font-size: 0.8em; text-align: center">
									Source: <a href="http://en.abna24.com/service/europe/archive/2016/04/25/749808/story.html">http://en.abna24.com/service/europe/archive/2016/04/25/749808/story.html</a>
								</p>

						<!-- Features -->
							<section id="features">
								<header class="style1">
									<h2>Sources of data and local focus</h2>
									<p>Data for the whole considered time frame was collected from Wikipedia and various other

										openly accessible websites.

										Additionally, data mostly covering the period from midst April 2016 until the beginning of June 2016 was collected from Twitter.

										Here, data was fetched separately in English, French, German, Italian and Spanish. For each language, there keywords as well as key actors were defined.
									<br> See below the four main sources for our data and a short description how we used them.</p>
								</header>
								<div class="feature-list">
									<div class="row">
										<div class="6u 12u(mobile)">
											<section>
												<h3 class="icon fa-twitter">Twitter Keywords</h3>
												<p>As twitter conveys a very good and broad overview of the sentiment in the population our main focus lays on fetching twitter keywords.</p>
											</section>
										</div>
										<div class="6u 12u(mobile)">
											<section>
												<h3 class="icon fa-google">Google | Websites</h3>
												<p>Through fetching websites we wanted to gain a rather neutral or expert view on our subject.</p>
											</section>
										</div>
									</div>
									<div class="row">
										<div class="6u 12u(mobile)">
											<section>
												<h3 class="icon fa-wikipedia-w">Wikipedia</h3>
												<p>As Wikipedia is constantly updating its sites we hoped to receive some time related overview of important events referring to refugees.</p>
											</section>
										</div>
										<div class="6u 12u(mobile)">
											<section>
												<h3 class="icon fa-area-chart">and Google Trends</h3>
												<p>Google Trends mainly helped us define our keywords for the search queries.</p>
											</section>
										</div>
									</div>
								</div>
							</section>

					</div>
				</div>
			<!-- Footer -->
				<div id="footer-wrapper" class="wrapper" style="padding-top: 0;">

					<?php include ("footer.php"); ?>

				</div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>