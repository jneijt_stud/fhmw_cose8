
<div id="logo">
    <h1>Coolhunting: Asylum Seekers</h1>
    <p>COIN 2016 | Team 8</p>
</div>

<div id="mainMenuBarAnchor"></div>
<nav id="nav">
    <ul>
        <li <?php if ($selected == "home") { echo 'class="current"'; } ?>><a href="index.php">the project</a></li>
        <li <?php if ($selected == "methodology") { echo 'class="current"'; } ?>><a href="methodology.php">methodology</a></li>
        <li <?php if ($selected == "timeline") { echo 'class="current"'; } ?>><a href="timeline.php">timeline</a></li>
        <li <?php if ($selected == "network") { echo 'class="current"'; } ?>><a href="network.php">network</a></li>

        <li <?php if ($selected == "mainresults" || $selected == "future") { echo 'class="current"'; } ?>>
            <a href="#">outcome</a>
            <ul>
                <li><a href="mainresults.php">main results</a></li>
                <li><a href="future.php">future prospects</a></li>
            </ul>
        </li>
        <li <?php if ($selected == "team") { echo 'class="current"'; } ?>><a href="theteam.php">the team</a></li>
    </ul>
</nav>
