/**
 * @author Joep Neijt <info@cockpit-online.org>
 */
data = null;
g = {en: new sigma.classes.graph(), de: new sigma.classes.graph()};
s = new sigma({
    graph: g.en,
    renderer: {
        container: 'graph-container',
        type: 'canvas',
        settings: {
            edgeColor: 'default',
            defaultEdgeColor: '#ccc',
            batchEdgesDrawing: true,
            hideEdgesOnMove: false
        }
    }
});

filter = sigma.plugins.filter(s);
fa = null;

var palette = {
    sent: { 7: ["#ff0000", "#bf0000", "#590000", "#000000", "#004a00", "#00a900", "#00ff00"] }
};
design = sigma.plugins.design(s);
design.setPalette(palette);

legend = sigma.plugins.legend(s);
graphInfoWidget = null;

function loadDataset() {
    var source = $('#formDataset input[name=network-source]:checked').val();
    var language = $('#formDataset input[name=network-language]:checked').val();
    var dataset = source + "-" + language;
    if (typeof dataset === "undefined") {
        return;
    }
    $('#graph-container').hide();
    $('#graph .message').html("Downloading dataset " + source + ", " + language);
    $('#graph .progress').html("0%");
    $('#graph .initNote').show();
    if (dataset.substr(0, 3) == "web") {
        $('form .no-web').hide();
    } else {
        $('form .no-web').show();
    }
    $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    $('#graph .progress').html(percentComplete + "%");
                }
            }, false);
            return xhr;
        },
        type: 'GET',
        url: "data/network-"+dataset+".json",
        data: {},
        cache: false,
        dataType: "json",
        success: function(d){
            data = d;
            console.log(data);
            $('#graph .initNote').hide();
            $('#graph-container').show();
            if (fa != null) {
                sigma.layouts.stopForceLink(s);
            }
            s.graph.clear();
            s.graph.read(d.data);
            s.refresh();

            setSize();
            setColors();

            fa = sigma.layouts.startForceLink(s, {
                autoStop: true,
                background: false,
                easing: 'cubicInOut',
                maxIterations: 50
            })
            .bind('interpolate', function(){
                s.settings('drawLabels', false);
                s.settings('drawEdgeLabels', false);
            })
            .bind('stop', function(){
                s.settings('drawLabels', true);
                s.settings('drawEdgeLabels', true);
                s.refresh({ skipIndexation:true });
            });

            legend.getWidget('node', 'size');
            legend.setPlacement('left');
            if (graphInfoWidget == null) {
                graphInfoWidget = legend.addTextWidget("");
            }
            graphInfoWidget.setText("Node count: " + data.info.nodeCount + " \n Edge count: "+data.info.edgeCount + " \n Minimum degree: " + data.info.minDegree);
        }
    });
}

function setSize() {
    var field = $("#formSize input[name='network-size']:checked").val();
    if (typeof field !== "undefined") {
        design.reset('nodes', 'size');
        design.nodesBy('size', {
            by: field,
            bins: 7,
            min: 0.05,
            max: 7
        }).apply();
        legend.draw();
    }
}

function setColors() {
    var field = $("#formColor input[name='network-color']:checked").val();
    if (typeof field !== "undefined") {
        design.reset('nodes', 'color');
        design.nodesBy('color', {
            by: field,
            bins: 7,
            scheme: 'sent'
        }).apply();
        legend.draw();
    }
}

$(document).ready(function() {
    $('#btnDownloadDataset').on("click", function(e) {
        loadDataset();
    });

    $("#formSize input[name='network-size']").on("change", function(e) {
        setSize();
    });

    $("#formColor input[name='network-color']").on("change", function(e) {
        setColors();
    });
});