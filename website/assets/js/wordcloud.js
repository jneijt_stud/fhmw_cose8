/**
 * @author Joep Neijt <info@cockpit-online.org>
 */
var Wordcloud = function() {
    var self = this;
    this.container = d3.select("#timeline-wordcloud");
    this.fill = d3.scale.linear().domain([0, 0.5, 1]).range(["red", "black", "green"]);
    this.date = new Date(Date.parse("2016-04-28T00:00:00+02:00"));
    this.wordCountMin = 0;
    this.wordCountMax = 0;
    this.maxFontSize = 200;
    this.data = [];
    this.max = null;
    this.fontSize = null;
    this.w = this.container.node().getBoundingClientRect().width;
    this.h = this.container.node().getBoundingClientRect().height;
    //this.w = window.innerWidth;
    //this.h = window.innerHeight;
    /*this.svg = d3.select("#timeline-wordcloud svg");
    console.log(this.svg);
    if (this.svg == null) {
        console.log("svdagsvaghdsa");*/
        this.svg = this.container.append("svg");
    //}
    this.vis = this.svg.append("g").attr("transform", "translate(" + [this.w >> 1, this.h >> 1] + ")");
    this.layout = d3.layout.cloud()
        .timeInterval(Infinity)
        .size([self.w, self.h])
        .rotate(0)
        .fontSize(function (d) {
            return self.fontSize(d.size);
        })
        .text(function (d) {
            return d.text;
        });
};
        
Wordcloud.prototype.load = function() {
    var language = $("#timeline-info input[name='wordcloud-data']:checked").val();
    var self = this;
    this.data = [];

    d3.json("data/wordcloud-"+language+".json", function (error, rawData) {
        if (error) throw error;
        rawData = rawData.filter(function (elem, index, arr) {
            return (new Date(Date.parse(elem.Date)) <= self.date);
        });
        wordcloud.vis.selectAll("text").remove()
        if (rawData.length > 0) {
            for (var i = 0; i < rawData[0].Words.length; i++) {
                rawData[0].Words[i].DateCount = 1.0;
            }
            var map = new d3.map(rawData[0].Words, function (elem) {
                return elem.Word;
            });


            for (var i = 1; i < rawData.length; i++) {
                var words = rawData[i].Words;
                words.forEach(function (elem, index, arr) {
                    if (map.has(elem.Word)) {
                        var word = map.get(elem.Word);
                        word.Sentiment += elem.Sentiment;
                        word.DateCount += 1.0;
                        map.set(elem.Word, word);
                    } else {
                        elem.DateCount = 1.0;
                        map.set(elem.Word, elem);
                    }
                });
            }

            map.forEach(function (key, value) {
                value.Sentiment = value.Sentiment / value.DateCount;
                if (self.wordCountMax < value.WordCount || self.wordCountMax == 0) {
                    self.wordCountMax = value.WordCount;
                }
                if (self.wordCountMin > value.WordCount || self.wordCountMin == 0) {
                    self.wordCountMin = value.WordCount;
                }
                self.data.push(value);
            });

            var logSize = d3.scale.log().range([0, 1]).domain([self.wordCountMin, self.wordCountMax]);
            var maxSize = 0;
            for (var i = 0; i < self.data.length; i++) {
                var size = logSize(self.data[i].WordCount) * self.data[i].Word.length;
                if (size > maxSize) {
                    maxSize = size;
                }
            }
        }

        self.maxFontSize = Math.ceil(window.innerWidth / (maxSize * 0.9));

        self.layout.font('impact').spiral('archimedean');
        self.fontSize = d3.scale.log().range([10, self.maxFontSize]).domain([self.wordCountMin, self.wordCountMax]);

        self.layout.stop().words(self.data.map(function (d) {
            return {text: d.Word, size: d.WordCount, sentiment: d.Sentiment};
        })).start();
    });
};

wordcloud = new Wordcloud();

function drawWordcloud (data, bounds) {
    var w = wordcloud.w, h = wordcloud.h;
    wordcloud.svg.attr("width", "100%").attr("height", "100%");
    wordcloud.svg.attr("viewBox", "0 0 " + w + " " + h);
    wordcloud.svg.attr("preserveAspectRatio", "xMidYMid meet");

    wordcloud.scale = bounds ? Math.min(
        w / Math.abs(bounds[1].x - w / 2),
        w / Math.abs(bounds[0].x - w / 2),
        h / Math.abs(bounds[1].y - h / 2),
        h / Math.abs(bounds[0].y - h / 2)) / 2 : 1;

    var text = wordcloud.vis.selectAll("text")
        .data(data, function (d) {
            return d.text.toLowerCase();
        });
    text.transition()
        .duration(1000)
        .attr("transform", function (d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .style("font-size", function (d) {
            return d.size + "px";
        });
    text.enter().append("text")
        .attr("text-anchor", "middle")
        .attr("transform", function (d) {
            return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
        })
        .style("font-size", function (d) {
            return d.size + "px";
        })
        .style("opacity", 1e-6)
        .transition()
        .duration(1000)
        .style("opacity", 1);
    text.style("font-family", function (d) {
            return d.font;
        })
        .style("fill", function (d) {
            return wordcloud.fill(d.sentiment);
        })
        .text(function (d) {
            return d.text;
        });
};