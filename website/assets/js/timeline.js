/**
 * @author Joep Neijt <info@cockpit-online.org>
 */
var container = d3.select("#timeline");
var padding = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 40
};
var w = container.node().getBoundingClientRect().width,
    h = container.node().getBoundingClientRect().height,
    innerWidth = w - padding.left - padding.right,
    innerHeight = h - padding.top - padding.bottom,
    x = d3.time.scale().range([0, innerWidth]),
    y = d3.scale.linear().range([0, innerHeight]),
    xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .tickSize(-innerHeight, 0)
        .tickPadding(6),
    yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .tickSize(-innerWidth,0)
        .tickPadding(6);

var types = {};
var fields = ["sentiment", "emotionality"];
var languages = ["de", "en", "fr", "it", "es"];
var colors = {
    de: {
        sentiment: "#00C5E5",
        emotionality: "#99001C"
    },
    en: {
        sentiment: "#2593DB",
        emotionality: "#B13415"
    },
    fr: {
        sentiment: "#4B62D2",
        emotionality: "#C9680E"
    },
    it: {
        sentiment: "#7131C8",
        emotionality: "#E19C07"
    },
    es: {
        sentiment: "#9700BF",
        emotionality: "#FAD000"
    }
};
fields.forEach(function(field, index, arr) {
    var type = {};
    type.line = d3.svg.line()
        .interpolate("linear")
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d[field]); });
    type.area = d3.svg.area()
        .interpolate("linear")
        .x(function(d) { return x(d.date); })
        .y0(innerHeight)
        .y1(function(d) { return y(d[field]); });
    types[field] = type;
});

var svg = d3.select("#timeline").append("svg:svg")
    .attr("width", w)
    .attr("height", h)
    .append("svg:g");

var rect = svg.append("svg:rect")
    .attr("class", "pane")
    .attr("width", innerWidth)
    .attr("height", innerHeight)
    .attr("transform", "translate(" + padding.left + "," + padding.top + ")");

svg.append("line")
    .attr("x1", padding.left).attr("y1", padding.top + (innerHeight / 2))
    .attr("x2", padding.left + innerWidth).attr("y2", padding.top + (innerHeight / 2))
    .attr("stroke-width", 2)
    .attr("stroke", "#aaa");

var graph = svg.append("svg:g")
    .attr("class", "graph-data")
    .attr("width", innerWidth)
    .attr("height", innerHeight)
    .attr("transform", "translate(" + padding.left + ", " + padding.top + ")");

var events = svg.append("svg:g")
    .attr("class", "graph-events")
    .attr("width", innerWidth)
    .attr("height", innerHeight)
    .attr("transform", "translate(" + padding.left + ", " + padding.top + ")");

svg.append("svg:rect")
    .attr("class", "bg-margin")
    .attr("width", padding.left)
    .attr("height", h);

svg.append("svg:rect")
    .attr("class", "bg-margin")
    .attr("width", padding.right)
    .attr("height", h)
    .attr("transform", "translate(" + (padding.left + innerWidth) + ", 0)");

svg.append("svg:g")
    .attr("class", "x axis")
    .attr("transform", "translate(" + padding.left + "," + (innerHeight + padding.top) + ")");

svg.append("svg:g")
    .attr("class", "y axis")
    .attr("transform", "translate(" + padding.left + ", " + padding.top + ")");

var legend = svg.append("g")
    .attr("width", 180)
    .attr("transform", "translate(" + (padding.left + innerWidth - 190) + ", " + padding.top +")");

legend.append("rect")
    .attr("class", "legend")
    .attr("width", 190)
    .attr("height", 140)
    .attr("stroke", "#aaa")
    .attr("stroke-width", 1);

fields.forEach(function (field, findex, farr) {
    legend.append("text")
        .attr("x", 7 + (findex * 90))
        .attr("y", 18)
        .attr("font-size",15)
        .text(field);

    languages.forEach(function(language, index, arr) {
        var x1 = 16 + (findex * 100),
            y1 = 35 + (index * 15),
            x2 = x1 + 30;
        legend.append("line")
            .attr("x1", x1).attr("y1", y1)
            .attr("x2", x2).attr("y2", y1)
            .attr("stroke-width", 3)
            .attr("stroke", colors[language][field]);

        legend.append("text")
            .attr("x", x2 + 10)
            .attr("y", y1 + 3)
            .attr("font-size",12)
            .text(language);
    });
});

legend.append("circle")
    .attr("class", "event")
    .attr("r", 10)
    .attr("cx", 27)
    .attr("cy", 120);

legend.append("text")
    .attr("x", 45)
    .attr("y", 125)
    .attr("font-size",12)
    .text("Clickable events");

var parseDate = d3.time.format("%a %b %d 00:00:00 CEST %Y").parse;
var parseEventDate = d3.time.format("%d %b %Y").parse;

x.domain([new Date(2016, 3, 17), new Date(2016, 5, 5)]);
y.domain([1,0]);

svg.call(d3.behavior.zoom().x(x).on("zoom", zoom));

draw();

languages.forEach(function(language, index, arr) {
    loadLanguage(language);
});
loadTimelineEvents();

function loadTimelineEvents() {
    d3.csv("data/timeline-events.csv", function(d) {
        return {
            date: parseEventDate(d.date),
            country: d.country,
            description: d.description
        };
    }, function (error, data) {
        if (error) throw error;
        events.selectAll("circle.event")
            .data(data)
            .enter().append("circle")
            .attr("class", "event")
            .attr("r", 10)
            .attr("cx", function(d) { return x(d.date); })
            .attr("cy", function(d) { return y(0.05); })
            .on("click", function(d, i) {
                eventClicked(d);
            });
    });
}

function loadLanguage(language) {
    d3.csv("data/sentiment-"+language+".csv", function (d) {
        if (d.Sentiment == "NaN" || d.Emotionality == "NaN" || d.Complexity == "NaN") {
            return "missing";
        }
        return {
            date: parseDate(d.Time),
            msgPerDay: +d["Avg. messages per day"],
            sentiment: +d.Sentiment,
            emotionality: +d.Emotionality,
            complexity: +d.Complexity
        };
    }, function (error, data) {
        if (error) throw error;

        var paths = [];
        var inpolPath = [[]];
        var current = [];
        data.forEach(function (elem, index, arr) {
            if (elem != "missing") {
                current.push(elem);
                inpolPath[0].push(elem);
            } else if (current.length > 0) {
                paths.push(current);
                current = [];
            }
        });
        if (current.length > 0) {
            paths.push(current);
        }

        fields.forEach(function (field, indes, arr) {
            var sentimentIntp = graph.selectAll("." + field + "-interpolation")
                .data(inpolPath)
                .enter()
                .append("g")
                .attr("class", field + "-" + language + "-interpolation");

            sentimentIntp.append("path")
                .attr("class", "line " + language + "-" + field + "-line")
                .attr("d", types[field].line)
                .style("stroke-width", "1")
                .style("stroke", colors[language][field])
                .style("stroke-dasharray", ("3, 3"));

            var sentiment = graph.selectAll("." + field)
                .data(paths)
                .enter()
                .append("g")
                .attr("class", field + "-" + language + "-data");

            sentiment.append("path")
                .attr("class", "line " + language + "-" + field + "-line")
                .style("stroke", colors[language][field])
                .attr("d", types[field].line);

            sentiment.append("path")
                .attr("class", "area " + language + "-" + field + "-area")
                .attr("clip-path", "url(#clip)")
                .style("fill", colors[language][field])
                .attr("d", types[field].area);
        });
    });
}

function draw() {
    svg.select("g.x.axis").call(xAxis);
    svg.select("g.y.axis").call(yAxis);
}

function zoom() {
    languages.forEach(function(language, index, arr) {
        fields.forEach(function(field, findex, farr) {
            svg.selectAll('path.' + language + "-" + field + '-line').attr('d', types[field].line);
            svg.selectAll('path.' + language + "-" + field + '-area').attr('d', types[field].area);
        });
    });
    svg.selectAll("circle.event")
        .attr("cx", function(d) { return x(d.date); })
        .attr("cy", function(d) { return y(0.05); });
    svg.select("g.x.axis").call(xAxis);
    svg.select("g.y.axis").call(yAxis);
    draw();
}

function selectLines() {
    var values = $("#timeline-controls input[name='timeline-data']:checked").map(function() {
        return this.value;
    }).get();
    var showAreas = $("#timeline-controls input[name='timeline-showAreas']:checked").length;
    languages.forEach(function(language, index, arr) {
        fields.forEach(function (field, findex, farr) {
            if (values.indexOf(field + '-' + language) >= 0) {
                svg.selectAll('path.' + language + "-" + field + '-line').style('stroke', colors[language][field]);
                if (showAreas) {
                    svg.selectAll('path.' + language + "-" + field + '-area').style('fill', colors[language][field]);
                } else {
                    svg.selectAll('path.' + language + "-" + field + '-area').style('fill', 'none');
                }
            } else {
                svg.selectAll('path.' + language + "-" + field + '-line').style('stroke', 'none');
                svg.selectAll('path.' + language + "-" + field + '-area').style('fill', 'none');
            }
        });
    });
}

function eventClicked(eventData) {
    var title = d3.time.format("%Y-%m-%d")(eventData.date);
    //wordcloud = new Wordcloud();
    wordcloud.date = eventData.date;
    wordcloud.load();

    if (eventData.country != "") {
        title = eventData.country + ", " + title;
    }
    $('.timeline-info-title', '#timeline-info').html(title);
    $('.timeline-info-description', '#timeline-info').html(eventData.description);
    $('#timeline-info').show();
}

wordcloud.layout.on("end", drawWordcloud);

$(document).ready(function() {
    $('#timeline-info').hide();
    $("#timeline-controls input[name='timeline-data']").on("change", function(e) {
        selectLines();
    });
    $("#timeline-controls input[name='timeline-showAreas']").on("change", function(e) {
        selectLines();
    });
    $("#timeline-info input[name='wordcloud-data']").on("change", function(e) {
        wordcloud.load();
    });

    wordcloud.load();
});