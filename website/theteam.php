<!DOCTYPE HTML>
<!--
	Escape Velocity by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Team :: COINS project team 8 - coolhunting about "asylum seekers"</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper" class="wrapper">
					<div id="header">

						<!-- Logo -->
						<!-- Nav -->
							<?php $selected="team"; include ("nav.php"); ?>

					</div>
				</div>

			<!-- Main -->
				<div class="wrapper style2">
					<div class="title">Who we are</div>
					<div id="main" class="container">

						<!-- Content -->
							<div id="content">
								<article class="box post">
									<header class="style1">
										<h2>Meet the team<br class="mobile-hide" /></h2>
										<!--
										<p>Tempus feugiat veroeros sed nullam dolore</p>
										-->
									</header>
									<!--
									<a href="#" class="image featured">
										<img src="images/pic01.jpg" alt="" />
									</a>
									-->
									<p style="text-align:center;">As follows we would like to present ourselves and the roles we obtained during the semester.</p>
									<!--
									<p>Phasellus nisl nisl, varius id porttitor sed, pellentesque ac orci. Pellentesque
									habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi
									bibendum justo sed mauris vehicula malesuada aliquam elit imperdiet. Aliquam eu nibh
									lorem, eget gravida mi. Duis odio diam, luctus et vulputate vitae, vehicula ac dolor.
									Pellentesque at urna eget tellus lobortis ultrices sed non erat. Donec eget erat non
									magna volutpat malesuada quis eget eros. Nullam sodales cursus sapien, id consequat
									leo suscipit ut. Praesent id turpis vitae turpis pretium ultricies. Vestibulum sit
									amet risus elit.</p>
									-->
								</article>
								<div class="row 150%">
									<div class="4u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Marco Bibrich</h2>
											</header>
											<a href="#" class="image featured"><img src="images/Marco.jpg" alt="" /></a>
											<p>Marco is our Twitter Fetch expert. Apart from that he also regularly checked Google Trends to compare and validate our keywords.</p>
											<!--
											<a href="#" class="button style1">More</a>
											-->
										</section>
									</div>
									<div class="4u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Robin Gieck</h2>
											</header>
											<a href="#" class="image featured"><img src="images/Robin.jpg" alt="" /></a>
											<p>Robin is the Wikipedia Fetch expert of the team. His tasks were to find out how useful Wikipedia would be with regard to the timeline data.</p>
										</section>
									</div>
									<div class="4u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Daniel Mayerhoffer</h2>
											</header>
											<a href="#" class="image featured"><img src="images/Daniel.jpg" alt="" /></a>
											<p>Daniel as the only non-IT person in the team is our analysis and data interpretation expert. He did most of the background check of the data.</p>
										</section>
									</div>
									<div class="row 150%">
										<div class="4u 12u(mobile)">
											<section class="box">
												<header>
													<h2>Joep Neijt</h2>
												</header>
												<a href="#" class="image featured"><img src="images/Joep.jpg" alt="" /></a>
												<p>Joep is our expert regarding most of the technical stuff like developing parsers etc. Therefore he implemented most of the interactive parts of the website.</p>
											</section>
										</div>
										<div class="4u 12u(mobile)">
											<section class="box">
												<header>
													<h2>Stephan Rabanser</h2>
												</header>
												<a href="#" class="image featured"><img src="images/Stephan.jpg" alt="" /></a>
												<p>Stephan is our Web Fetch expert and belongs to the Apple-minority in our team. He also contacted a NGO to see whether a cooperation would be possible.</p>
											</section>
										</div>
										<div class="4u 12u(mobile)">
											<section class="box">
												<header>
													<h2>Annika Winterhalter</h2>
												</header>
												<a href="#" class="image featured"><img src="images/Annika.jpg" alt="" /></a>
												<p>Annika is our expert in maintaining the website. During the project she helped out wherever an additional pair of hands was needed.</p>
											</section>
										</div>
									</div>
								</div>
							</div>

					</div>
				</div>

			<!-- Highlights -->
			<!--
				<div class="wrapper style3">
					<div class="title">The Endorsements</div>
					<div id="highlights" class="container">
						<div class="row 150%">
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
									<h3><a href="#">Aliquam diam consequat</a></h3>
									<p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
									<ul class="actions">
										<li><a href="#" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
									<h3><a href="#">Nisl adipiscing sed lorem</a></h3>
									<p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
									<ul class="actions">
										<li><a href="#" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="#" class="image featured"><img src="images/pic04.jpg" alt="" /></a>
									<h3><a href="#">Mattis tempus lorem</a></h3>
									<p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
									<ul class="actions">
										<li><a href="#" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
						</div>
					</div>
				</div>
				-->

			<!-- Footer -->
			<div id="footer-wrapper" class="wrapper" style="padding-top: 0;">
				<!--
                <div class="title">The Rest Of It</div>
                <div id="footer" class="container">
                    <header class="style1">
                        <h2>Ipsum sapien elementum portitor?</h2>
                        <p>
                            Sed turpis tortor, tincidunt sed ornare in metus porttitor mollis nunc in aliquet.<br />
                            Nam pharetra laoreet imperdiet volutpat etiam consequat feugiat.
                        </p>
                    </header>
                    <hr />

                </div>
                -->

					<?php include ("footer.php"); ?>

				</div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>