<!DOCTYPE HTML>
<!--
	Escape Velocity by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Future prospects :: COINS project team 8 - coolhunting about "asylum seekers"</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper" class="wrapper">
					<div id="header">

						<!-- Logo -->
						<!-- Nav -->
							<?php $selected="future"; include ("nav.php"); ?>

					</div>
				</div>

			<!-- Main -->
				<div class="wrapper style2">
					<div class="title">Future applicability</div>
					<div id="main" class="container">

						<!-- Content -->
							<div id="content">
								<article class="box post">
									<header class="style1">
										<h2>The potential of our project</h2>
										<p>What could the project be useful for? And what are possible advancements?</p>
									</header>
									<header class="style2" style="text-align: center">
										<h2>Practical applicability</h2>
									</header>
									<p>With our main question is focusing on the sentiment and perception of regufees and asylum seekers in Europe,
									the collected data shows where for example weak spots or more generally strategic focal points for a certain topic can be found. And the data also shows in which countries or language clusters the sentiment might be rather good or bad and why. Are newspapers only reporting
									problems with refugees? Or are the newspapers not even read? Or are there certain groups in the network which have a high influence but are not influenced from
									anyone on the outsite? All in all making it much easier to approach certain topics in public and also providing a way of targeting specific groups with information
									needed specifically for them.</p>
									<p>One way of using this information would for example be in politics target campaigns better at a certain group in the network by first finding out which group in the
										network one wants to reach (for example by sentiment) and then directly targeting the key actors for that group. This would allow for much faster and more reliable
										distribution of information within the network. And of course this could (and should) not only be used in politics to change the idea of someone, but also to just
										spread factual information. For example NGO's like <a href="https://newsthatmoves.org/en/">News.That.Moves</a> could target the key actors of certain key groups to inform them of the current situation in their
										field of interest.</p>
									<p>Of course the data also offers possibilities to further research the underlying conflicts of interest of the refugee crisis. Why are some people so negative about
										refugees? How do others want to help refugees? How do the two sides influence each other, is there any communication between them, or do they only talk to their peers?<br>
										All questions which could maybe be answered at some point and could help prevent the situation of escalating.</p>

									<header class="style2" style="text-align: center;">
										<h2>Possible advancements</h2>
									</header>
									<p>At the beginning of the project phase the question was raised how it might be possible to prevent terrorism by analysing data fetched from the internet.
									As we know secret services already use big data analysis to find out more about terror cells or planned terror attacks and were successful with this method already.
										Nevertheless, we doubted that we might be able to find any answer or even solution that would approach the subject of terrorism appropriately in our one semester project.
									But in case of further development we think the way we conducted the data analysis could be helpful concerning the exposure and prevention of e.g. regional attacks on refugee asylums
									such as it happens in Germany for quite a while now (see e.g. <a href="http://www.nzz.ch/international/deutschland-und-oesterreich/fremdenfeindliche-vorfaelle-in-sachsen-im-hinterhof-der-willkommenskultur-ld.5565">NZZ | 21.2.2016)</a>.
									The analysis of the sentiment allows to gain an insight in how positive or negative the discourse is in one particular region and what are the topics that affect the people.
										In case there is a very negative sentiment and a trend to racist and violent statements this could be taken as a cause to take actions to prevent any violent attacks on refugees.
										So in at least a small and regional area not necessarily terrorism but the application of violence could be prevented through analysing the sentiment.
									</p>

									<!--<ul class="fa-ul">
										<li><i class="fa-li fa fa-arrow-circle-o-right"></i> <a href="http://moving-europe.org/">Moving Europe</a></li>
										<li><i class="fa-li fa fa-arrow-circle-o-right"></i> <a href="http://watchthemed.net/">Watch the Med</a></li>
										<li><i class="fa-li fa fa-arrow-circle-o-right"></i> <a href="https://alarmphone.org/en/">Alarmphone</a></li>
										<li><i class="fa-li fa fa-arrow-circle-o-right"></i> <a href="https://newsthatmoves.org/en/">News.That.Moves</a></li>
									</ul>
									<p>These non-profit and non-governmental organisations all aim at supporting refugees and asylum seekers, mainly when they are already on their way to Europe
										or arrive at their target location.</p>-->
								</article>
								</div>
							</div>

					</div>
				</div>

			<!-- Highlights -->
				<div class="wrapper style3">
					<div class="title">Summary</div>
					<div id="highlights" class="container">
						<header class="style1" style="text-align: center; padding-top: 0;">
							<h2>Raise awareness</h2>
						</header>
						<p>As we can see in all those ideas and proposals, one thing seems to be essential: The communication and transportation of valid and prevailing information to the people.
							Often, a conflict or sometimes also a negative sentiment seems to be a consequence of insufficient or wrong information. But by using our data, the key actors and sentiments
							of all the different groups, we could improve the way we deliver information to the people.</p>
						<p>It should not be the fear of something unknown that drives people, but their own willingness to do something one way or the other.</p>
						<p>Some NGOs have started to gather more detailed and more reliable data on the current situation and are trying to distribute this information over new channels, using
							interactivity and social media. Some of them, we have listed below as examples. Please feel free to explore them yourselves, these are obviously also not the only ones out
							there.</p>
						<div class="row 150%">
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="http://moving-europe.org/" class="image featured"><img src="images/moving-europe.png" alt="" /></a>
									<h3><a href="http://moving-europe.org/">Moving Europe</a></h3>
									<ul class="actions">
										<li><a href="http://moving-europe.org/" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="http://watchthemed.net/" class="image featured"><img src="images/watch-the-med.png" alt="" /></a>
									<h3><a href="http://watchthemed.net/">Watch the Med</a></h3>
									<ul class="actions">
										<li><a href="http://watchthemed.net/" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="https://alarmphone.org/en/" class="image featured"><img src="images/alarmphone.png" alt="" /></a>
									<h3><a href="https://alarmphone.org/en/">Alarmphone</a></h3>
									<ul class="actions">
										<li><a href="https://alarmphone.org/en/" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
						</div>
					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper" class="wrapper" style="padding-top: 0;">


					<?php include ("footer.php"); ?>

				</div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>