    <div id="copyright" style="margin-top: 0;">
        <section class="feature-list small">
            <div class="row">
                <div class="12u 12u(mobile)">
                    <section>
                        <h3 class="icon fa-envelope">Email</h3>
                        <p id="footer-paragraph">
                            <a href="mailto:coinproject8@gmail.com">coinproject8@gmail.com</a>
                        </p>
                    </section>
                </div>
            </div>
        </section>
        <ul>
            <li>&copy; Team8-cose2016</li><li><a href="impressum.php">Impressum/Imprint</a></li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
        </ul>
    </div>