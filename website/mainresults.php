<!DOCTYPE HTML>
<!--
	Escape Velocity by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
    <title>Main results :: COINS project team 8 - coolhunting about "asylum seekers"</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css"/><![endif]-->
</head>
<body class="no-sidebar">
<div id="page-wrapper">

    <!-- Header -->
    <div id="header-wrapper" class="wrapper">
        <div id="header">

            <!-- Logo -->
            <!-- Nav -->
            <?php $selected = "mainresults";
            include("nav.php"); ?>

        </div>
    </div>

    <!-- Main -->
    <div class="wrapper style2">
        <div class="title">Main Results</div>
        <div id="main" class="container">

            <!-- Content -->
            <div id="content">
                <article class="box post">
                    <header class="style1">
                        <h2>Main results and key actors</h2>
                        <p>In general most key actors are more or less positive towards the refugee crisis, suggesting that the negative sentiment seen in the word clouds reflects the ideas of a lot of
                        smaller actors or is due to the problems the refugees have themselves. Interestingly, the german and english wordclouds are much more negative than those of the other three
                        languages.</p>
                    </header>
                    <header class="style2" style="text-align: center">
                        <h2>General overview and defining properties for key actors</h2>
                    </header>
                    <p>Two parameters play a crucial role in estimating the importance of an actor in the debate.
                        Firstly <span style="font-style: italic;">betweenness centrality</span>
                        matters by indicating how often an actor's post is re-tweeted or how often the actor re-tweets
                        posts by others. But a high number of posts alone does
                        not imply great importance. However, consider as an extreme case a Twitter account without a
                        single follower constantly retweeting posts of others.
                        Since <span style="font-style: italic;">betweenness centrality</span> has been calculated for
                        undirected nodes, this value of such an account without any actual influence
                        would be extremely high. Thus, secondly the <span style="font-style: italic;">number of followers</span>
                        is central in order to determine how important an actor is. So what
                        do combinations of the two values mean?</p>

                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-arrow-circle-o-right"></i> High betweenness centrality and many
                            followers (example: <span style="font-style: italic;">@refugees</span>,
                            <span style="font-style: italic;">@spiegelonline</span>): This is a very important account
                            in the debate which posts a considerable
                            amount of information (maybe from different sources) that is shared by many and read
                            by even more. Accounts of some news pages or of big organisations con-
                            cerned about refugees belong to this group. Their posts have a very high impact
                            (per post and because of their frequency).
                        </li>
                        <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Medium to low betweenness centrality and
                            many followers (example: <span style="font-style: italic;">@zeitonline</span>):

                            Account posting about numerous different topics and occasionally informing about

                            refugees, which is then often read and shared. Thus, if they post something, this

                            has a high impact. Many general news pages share such features.
                        </li>
                        <li><i class="fa-li fa fa-arrow-circle-o-right"></i> High betweenness centrality and few to very
                            few followers (example: <span style="font-style: italic;">@XYEinzelfall</span>,

                            <span style="font-style: italic;">@asyl news</span>): Small accounts devoted solely to the
                            so-called refugee crisis and dedicated to advocating their position.
                            Often, this is done by retweeting a selection of

                            news posted by others and mixing that with one's own comments. Usually private

                            accounts created to comment the crisis belong to this group. Their overall impact is limited
                            (and is only created by retweets by their followers)
                            but they highly influence those who generally share their opinion by reinforcing their
                            beliefs. This

                            may eventually lead to a higher segregation of the debate (as seen in Germany

                            where some members of the right-wing stopped listening to the "Lügenpresse" and

                            receive news only from their peers now).
                        </li>
                        <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Very low betweenness centrality and very
                            few followers: Accounts of people who just follow
                            the debate and do not actively participate but still retweet posts
                            they strongly agree (or disagree) with.
                        </li>
                    </ul>


                    <p>It is certainly hard to distinguish between the different groups (especially between

                        the first two) but they can give an impression of how a certain actor participates and
                        influences the debate. The <a href="network.php">networks graphs</a>

                        can be looked at from the perspective of betweenness centrality as well as from the

                        perspective of followers count.</p>
                </article>

                <article class="box post">
                    <header class="style2" style="text-align: center">
                        <h2>Google/Web results</h2>
                    </header>
                    <p>Overall we can observe a rather positive sentiment throughout the european countries when looking
                        at the web results. This can result from
                        the fact that bloggers are considered more educated in contrast to the entire crowd being
                        represented in social networks, like Twitter. Also,
                        it is probably easier to complain about topics in 140 characters, while blog posts are usually
                        longer and the blogging community frequently encourages
                        writers to elaborate their ideas thoroughly and to conduct at least a little research on the
                        topic they are writing about. Still, some authors also
                        publish negative columns on both small and major websites, impacting the sentiment score.</p>

                    <p>In contrast to the word cloud, the network itself shows a rather negative sentiment. While
                        negative words seem to be more prominent in our fetches
                        than positive words in general, words directly related to our search terms seem to be more
                        concentrated on a smaller amount of words which in
                        turn causes different distributions for the network (overall negative) and the word cloud
                        (overall negative).</p>
                </article>

                <article class="box post">
                    <header class="style2" style="text-align: center">
                        <h2>Twitter results</h2>
                    </header>
                    <div style="text-align: center"><b>Choose a language</b>
                        <!-- Selection -->
                        <form action="" id="actors_form">
                            <span style="padding-right: 60px;"><input type="radio" name="actors-data" value="en"
                                                                      checked="checked"> English</span>
                            <span style="padding-right: 60px;"><input type="radio" name="actors-data" value="de"> German</span>
                            <span style="padding-right: 60px;"><input type="radio" name="actors-data" value="fr"> French</span>
                            <span style="padding-right: 60px;"><input type="radio" name="actors-data" value="es"> Spanish</span>
                            <span style="padding-right: 60px;"><input type="radio" name="actors-data" value="it"> Italian</span>
                            <span><input type="radio" name="actors-data" value="summary"> Summary</span>
                        </form>
                    </div>

                    <hr/>

                    <div id="actor_divs">
                        <div id="actors_en" class="actors">
                            <h2 style="text-align: center; margin-bottom: 2em;">English</h2>
                            <hr/>

                            <div class="row 150%">
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@TheEconomist</h2>
                                        </header>

                                        <a href="https://twitter.com/TheEconomist" target="_blank"
                                           class="image featured"><img src="images/theEconomist.jpg" alt=""/></a>
                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the British weekly newspaper <span
                                                    style="font-style: italic;">The Economist</span></li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (holding pluralistic, libertarian positions)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Advocating a liberal
                                                refugee and immigration policy
                                            </li>
                                        </ul>

                                        <!--
                                        <a href="#" class="button style1">More</a>
                                        -->
                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@refugees</h2>
                                        </header>
                                        <a href="https://twitter.com/Refugees" target="_blank"
                                           class="image featured"><img src="images/refugeesUNHCRtwitter.jpg"
                                                                       alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span style="font-style: italic;">UNITED NATION Refugee Agency (UNHCR)</span>
                                                <ul class="fa-ul" style="margin-bottom: 0;">
                                                    <li><i class="fa-li fa fa-arrow-circle-o-right"
                                                           style="color: #b6b6b6;"></i> United Nations programme
                                                        mandated to protect and support refugees
                                                    </li>
                                                    <li style="padding-bottom: 0;"><i
                                                            class="fa-li fa fa-arrow-circle-o-right"
                                                            style="color: #b6b6b6;"></i> Assists in voluntary
                                                        repatriation of refugees as well as their local integration
                                                        or resettlement to another country
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Aims on heighten
                                                awareness of the issues faced by refugees around the world
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@UN</h2>
                                        </header>
                                        <a href="https://twitter.com/UN" target="_blank" class="image featured"><img
                                                src="images/unTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span style="font-style: italic;">United Nations Organisation</span>
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Informs about important
                                                events all around the world
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Pursues a
                                                supra-national agenda
                                            </li>
                                        </ul>

                                        <!--
                                        <a href="#" class="button style1">More</a>
                                        -->
                                    </section>
                                </div>
                            </div>

                            <div class="row 150%">
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@guardian</h2>
                                        </header>
                                        <a href="https://twitter.com/guardian" target="_blank"
                                           class="image featured"><img src="images/guardianTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the British daily newspaper <span
                                                    style="font-style: italic;">The Guardian</span></li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (holding left-wing positions)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Advocating a liberal
                                                refugee policy
                                            </li>
                                        </ul>
                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@hrw</h2>
                                        </header>
                                        <a href="https://twitter.com/hrw" target="_blank" class="image featured"><img
                                                src="images/hrwTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of <span style="font-style: italic;">Human Rights Watch</span>
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Organisation opposing
                                                violations of what it considers basic human rights (in per-
                                                suasion to the Universal Declaration of Human Rights)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Promoting refugees'
                                                rights and advocating their decent treatment as well as liberal
                                                acceptance criteria
                                            </li>
                                        </ul>
                                        <!--
                                        <a href="#" class="button style1">More</a>
                                        -->
                                    </section>
                                </div>
                            </div>

                            <!--<h3>Summary</h3>

                            <p>All European or international key actors favour a liberal asylum policy and their mind-
                                set concerning refugees is positive. Against this background, the overall very negative
                                sentiment indicates that there many smaller actors complaining about refugees.
                            </p>
                            -->

                        </div>


                        <div id="actors_de" class="actors">
                            <h2 style="text-align: center; margin-bottom: 2em;">German</h2>
                            <hr/>

                            <div class="row 150%">
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@SPIEGELONLINE</h2>
                                        </header>
                                        <a href="https://twitter.com/SPIEGELONLINE" class="image featured"><img
                                                src="images/sponTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the news site <span style="font-style: italic;">Spiegel Online</span>
                                                (associated with the weakly
                                                journal <span style="font-style: italic;">SPIEGEL</span>)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (holding pluralistic, left-wing positions)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Advocating a liberal
                                                refugee and immigration policy
                                            </li>
                                        </ul>

                                        <!--
                                        <a href="#" class="button style1">More</a>
                                        -->
                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@tagesschau</h2>
                                        </header>
                                        <a href="https://twitter.com/tagesschau" class="image featured"><img
                                                src="images/tagesschauTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span style="font-style: italic;">Tagesschau</span></li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (politically neutral or slightly left-wing positions)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No clear agenda
                                                regarding refugees
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@welt</h2>
                                        </header>
                                        <a href="https://twitter.com/welt" class="image featured"><img
                                                src="images/weltTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of German daily newspaper <span style="font-style: italic;">Die Welt</span>
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (holding conservative and libertarian positions)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Focusing on economic
                                                questions regarding refugees, diverse overall position
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="row 150%">
                                    <div class="4u 12u(mobile)">
                                        <section class="box">
                                            <header>
                                                <h2>@zeitonline</h2>
                                            </header>
                                            <a href="https://twitter.com/zeitonline" class="image featured"><img
                                                    src="images/zeitonlineTwitter.jpg" alt=""/></a>

                                            <ul class="fa-ul">
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                    account of the news site <span style="font-style: italic;">Zeit Online</span>
                                                    (associated with the weakly
                                                    paper <span style="font-style: italic;">ZEIT)</span></li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                    (holding libertarian positions), famous for its multi-perspective
                                                    debates
                                                </li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No clear agenda
                                                    regarding refugees, optimistic contributions as well as sceptical
                                                    ones
                                                </li>
                                            </ul>

                                        </section>
                                    </div>
                                    <div class="4u 12u(mobile)">
                                        <section class="box">
                                            <header>
                                                <h2>@XYEinzelfall</h2>
                                            </header>
                                            <a href="https://twitter.com/XYEinzelfall" class="image featured"><img
                                                    src="images/xyeinzelTwitter.jpg" alt=""/></a>

                                            <ul class="fa-ul">
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> German private
                                                    account aiming to point out "crimes by refugees and immigrants"
                                                </li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Posting police
                                                    reports and news concerning criminal refugees as well as occasional
                                                    political statements
                                                </li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Clearly critical of
                                                    refugees and promoting a restrictive refugee policy
                                                </li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Only few (less than
                                                    5000) followers
                                                </li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> High frequency of
                                                    tweets: 50 per day
                                                </li>
                                            </ul>

                                        </section>
                                    </div>
                                    <div class="4u 12u(mobile)">
                                        <section class="box">
                                            <header>
                                                <h2>@asyl_news</h2>
                                            </header>
                                            <a href="https://twitter.com/asyl_news" class="image featured"><img
                                                    src="images/asyl_newsTwitter.jpg" alt=""/></a>

                                            <ul class="fa-ul">
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Private account
                                                    twittering exclusively in German language with sole purpose: “pro-
                                                    mote solidarity with refugees and intervene against xenophobia &
                                                    racism”
                                                </li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Only few (less than
                                                    1000) followers
                                                </li>
                                                <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Very high frequency
                                                    of tweets (mainly re-tweets): more than 100 per day
                                                </li>
                                            </ul>

                                        </section>
                                    </div>
                                </div>
                            </div>

                            <!--<h3>Summary</h3>

                            <p>Small accounts are more likely to present distinct political views and they become im-
                                portant in the network because of the frequency of their tweets rather than the
                                followers
                                who are (directly) reached.
                            </p>
                            -->
                        </div>


                        <div id="actors_fr" class="actors">
                            <h2 style="text-align: center; margin-bottom: 2em;">French</h2>
                            <hr/>

                            <div class="row 150%">
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@francetvinfo</h2>
                                        </header>
                                        <a href="https://twitter.com/francetvinfo" class="image featured"><img
                                                src="images/francetvTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the news section of <span style="font-style: italic;">France Télévisions</span>,
                                                the French
                                                public national television broadcaster
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (neutral to slightly left-wing positions)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No clear agenda
                                                regarding refugees
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@le_Parisien</h2>
                                        </header>
                                        <a href="https://twitter.com/le_Parisien" class="image featured"><img
                                                src="images/parisienTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the news site <span
                                                    style="font-style: italic;">Le Parisien</span></li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (neutral to slightly left-wing positions)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> High frequency of
                                                tweets: 50 per day
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@Blancheurope</h2>
                                        </header>
                                        <a href="https://twitter.com/Blancheurope" class="image featured"><img
                                                src="images/blancheuropeTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Openly racist French
                                                account agitating against the EU as well as foreigners (espe-
                                                cially non-European ones)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Only few (about 1000)
                                                followers
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                            </div>
                        </div>

                        <div id="actors_es" class="actors">
                            <h2 style="text-align: center; margin-bottom: 2em;">Spanish</h2>
                            <hr/>

                            <div class="row 150%">
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@bbcmundo</h2>
                                        </header>
                                        <a href="https://twitter.com/bbcmundo" class="image featured"><img
                                                src="images/bbcmundoTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the Spanish section of <span style="font-style: italic;">BBC World Service</span>,
                                                the world's
                                                largest international broadcaster
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news page
                                                (non-party)
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No clear agenda
                                                regarding refugees
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@unicef_es</h2>
                                        </header>
                                        <a href="https://twitter.com/unicef_es" class="image featured"><img
                                                src="images/unicef_esTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span style="font-style: italic;">United Nations Children's Fund (UNICEF)</span>
                                                in Spain
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> News about all kinds of
                                                situations where children need help and measures taken
                                                by UNICEF
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Raising awareness for
                                                problems of refugee children
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                            </div>
                        </div>

                        <div id="actors_it" class="actors">
                            <h2 style="text-align: center; margin-bottom: 2em;">Italian</h2>
                            <hr/>

                            <div class="row 150%">
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@repubblicait</h2>
                                        </header>
                                        <a href="https://twitter.com/repubblicait" class="image featured"><img
                                                src="images/repubblicaTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span style="font-style: italic;">la Repubblica</span>
                                                newspaper
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General newspaper,
                                                center-left political position
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No official agenda
                                                regarding refugees, although a rather more positive attitude
                                                towards refugees could be assumed
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@SkyTG24</h2>
                                        </header>
                                        <a href="https://twitter.com/SkyTG24" class="image featured"><img
                                                src="images/skyTG24Twitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span style="font-style: italic;">Sky TG24</span> news
                                                channel
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General news channel
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No official agenda
                                                regarding refugees
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@radiodeejay</h2>
                                        </header>
                                        <a href="https://twitter.com/radiodeejay" class="image featured"><img
                                                src="images/radiodeejayTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span style="font-style: italic;">Radio Deejay</span>
                                                radio channel belonging to the <span style="font-style: italic;">Gruppo
														Editoriale L’Espresso</span></li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General radio channel,
                                                center-left political position
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No official agenda
                                                regarding refugees, although a rather more positive attitude
                                                towards refugees could be assumed
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                            </div>
                            <div class="row 150%">
                                <div class="4u 12u(mobile)">
                                    <section class="box">
                                        <header>
                                            <h2>@corriere</h2>
                                        </header>
                                        <a href="https://twitter.com/Corriere" class="image featured"><img
                                                src="images/corriereTwitter.jpg" alt=""/></a>

                                        <ul class="fa-ul">
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> Official Twitter
                                                account of the <span
                                                    style="font-style: italic;">Corriere della Sera</span> newspaper
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> General newspaper,
                                                center-right political position
                                            </li>
                                            <li><i class="fa-li fa fa-arrow-circle-o-right"></i> No official agenda
                                                regarding refugees, although a rather more negative attitude
                                                towards refugees could be assumed
                                            </li>
                                        </ul>

                                    </section>
                                </div>
                            </div>
                        </div>

                        <div id="actors_summary" class="actors">

                            <h3>Overall summary concerning key actors</h3>
                            <p> The Twitter accounts of news sites play an important role and so do international

                                organisations (especially those under the aegis of the United Nations). Interestingly,

                                government institutions or political parties play no extraordinary role in the refugee

                                debate. Instead, some small accounts, often created to only tweet about the refugee

                                crisis (or even only an aspect of it) are very central in the networks. However, those

                                accounts often have few followers while posting in a very high frequency (in average at

                                least more than one post per hour) on their topic.

                                Most key actors have a mindset concerning refugees which is positive. However, some

                                of them mention problems which refugees have which might be one reason for the overall

                                negative sentiment in all languages. Nevertheless, this negative sentiment cannot be

                                explained by that alone: There are many smaller actors complaining about refugees and

                                demanding more restrictive asylum policies.</p>
                            <p>
                                Additionally, political parties in europe generally pay only little attention to social media. They usually do not have specialised teams
                                for social media within their PR-departments which leads to a weak performance in this sector quantitatively as well as qualitatively (regarding
                                the appeal of their posts to be received or retweeted). Furthermore, bonds with their intended receivers are weak because e.g. no personal
                                information about politicians or other non-politics related content is provided. This explains, why their role in the debate is limited.
                            </p>

                            <h3>Sentiment</h3>
                            <p>In general, a negative sentiment does not necessarily imply an overall negative opinion

                                about a phrase in the context of refugees, since problems can be problems of refugees

                                as well as problems with refugees.</p>

                        </div>
                    </div>


                </article>
            </div>
        </div>

    </div>
</div>

<div class="wrapper style3">
    <div class="title">Interested in the data?</div>
    <div id="highlights" class="container">
        <header class="style1" style="text-align: center; padding-top: 0;">
            <p>
                In case you are interested in the datasets themselves, you may download all of them here. We provide them as CSV files with actors and edges separate for higher compatibility with other analysis tools.
            </p>
        </header>
        <div class="row 150%">
            <div class="4u 12u(mobile)">
                <section class="highlight">
                    <h3>Twitter</h3>
                    <ul>
                        <li>English: <a href="data/datasets/TwitterActors_en.csv">CSV actors</a> | <a href="data/datasets/TwitterLinks_en.csv">CSV edges</a></li>
                        <li>German: <a href="data/datasets/TwitterActors_de.csv">CSV actors</a> | <a href="data/datasets/TwitterLinks_de.csv">CSV edges</a></li>
                        <li>French: <a href="data/datasets/TwitterActors_fr.csv">CSV actors</a> | <a href="data/datasets/TwitterLinks_fr.csv">CSV edges</a></li>
                        <li>Italian: <a href="data/datasets/TwitterActors_it.csv">CSV actors</a> | <a href="data/datasets/TwitterLinks_it.csv">CSV edges</a></li>
                        <li>Spanish: <a href="data/datasets/TwitterActors_es.csv">CSV actors</a> | <a href="data/datasets/TwitterLinks_es.csv">CSV edges</a></li>
                    </ul>
                </section>
            </div>
            <div class="4u 12u(mobile)">
                <section class="highlight">
                    <h3>Web</h3>
                    <ul>
                        <li>English: <a href="data/datasets/WebActors_en.csv">CSV actors</a> | <a href="data/datasets/WebLinks_en.csv">CSV edges</a></li>
                        <li>German: <a href="data/datasets/WebActors_de.csv">CSV actors</a> | <a href="data/datasets/WebLinks_de.csv">CSV edges</a></li>
                        <li>French: <a href="data/datasets/WebActors_fr.csv">CSV actors</a> | <a href="data/datasets/WebLinks_fr.csv">CSV edges</a></li>
                        <li>Italian: <a href="data/datasets/WebActors_it.csv">CSV actors</a> | <a href="data/datasets/WebLinks_it.csv">CSV edges</a></li>
                        <li>Spanish: <a href="data/datasets/WebActors_es.csv">CSV actors</a> | <a href="data/datasets/WebLinks_es.csv">CSV edges</a></li>
                    </ul>
                </section>
            </div>
            <div class="4u 12u(mobile)">
                <section class="highlight">
                    <h3>Wikipedia</h3>
                    <ul>
                        <li>English: <a href="data/datasets/WikipediaActors_en.csv">CSV actors</a> | <a href="data/datasets/WikipediaLinks_en.csv">CSV edges</a></li>
                        <li>German: <a href="data/datasets/WikipediaActors_de.csv">CSV actors</a> | <a href="data/datasets/WikipediaLinks_de.csv">CSV edges</a></li>
                        <li>Italian: <a href="data/datasets/WikipediaActors_it.csv">CSV actors</a> | <a href="data/datasets/WikipediaLinks_it.csv">CSV edges</a></li>
                        <li>Spanish: <a href="data/datasets/WikipediaActors_es.csv">CSV actors</a> | <a href="data/datasets/WikipediaLinks_es.csv">CSV edges</a></li>
                    </ul>
                </section>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<div id="footer-wrapper" class="wrapper" style="padding-top: 0;">
    <?php include("footer.php"); ?>
</div>
</div>

<!-- Scripts -->

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.dropotron.min.js"></script>
<script src="assets/js/skel.min.js"></script>
<script src="assets/js/skel-viewport.min.js"></script>
<script src="assets/js/util.js"></script>
<!--[if lte IE 8]>
<script src="assets/js/ie/respond.min.js"></script><![endif]-->
<script src="assets/js/main.js"></script>

<script type="text/javascript">

    var $divs = $('#actor_divs > div');
    $divs.first().show()


    $(document).ready(function () {


        $('input[type="radio"]').click(function () {


            if ($(this).attr("value") == "en") {

                $(".actors").not("#actors_en").hide();

                $("#actors_en").show();

            }

            if ($(this).attr("value") == "de") {

                $(".actors").not("#actors_de").hide();

                $("#actors_de").show();

            }

            if ($(this).attr("value") == "fr") {

                $(".actors").not("#actors_fr").hide();

                $("#actors_fr").show();

            }

            if ($(this).attr("value") == "es") {

                $(".actors").not("#actors_es").hide();

                $("#actors_es").show();

            }

            if ($(this).attr("value") == "it") {

                $(".actors").not("#actors_it").hide();

                $("#actors_it").show();

            }

            if ($(this).attr("value") == "summary") {

                $(".actors").not("#actors_summary").hide();

                $("#actors_summary").show();

            }

        });

    });

</script>

</body>
</html>