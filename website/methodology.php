<!DOCTYPE HTML>
<!--
	Escape Velocity by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Methodology :: COINS project team 8 - coolhunting about "asylum seekers"</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="left-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
			<div id="header-wrapper" class="wrapper">
				<div id="header">
					<!-- Nav -->
					<?php $selected="methodology"; include ("nav.php"); ?>
				</div>
			</div>

			<!-- Main -->
			<div class="wrapper style2">
				<div class="title">Methodical approach</div>
				<div id="main" class="container">
					<div class="row 150%">
						<div class="4u 12u(mobile)">

							<!-- Sidebar -->
							<div id="sidebar">
								<section class="box">
									<header>
										<h2>Our technical achievements</h2>
										<hr>
									</header>
									<article class="box">
										<h3>Word Cloud Parser</h3>
										<img style="max-width: 100%;" src="images/word-cloud-parser.jpg" alt="" />
										<p>The data coming from the condor software in CSV format was parsed by a specially developed console tool to bring it into a format we could use to implement the wordcloud on the website. This parser also does quite a lot of processing beforehand to keep load times of the website due to excessive data processing to a minimum.</p>
										<p>Also, the data was limited to a maximum of about 40 words as there is hardly any need for more data and this would only increase the file size.</p>
									</article>

									<article class="box">
										<h3>Network Visualisation</h3>
										<img style="max-width: 100%;" src="images/network-visualisation.png" alt="" />
										<p>Using the amazing linkurious javascript library, we were able to add our fetched networks to the website offering the visitor an interactive way to explore the data. Again, to limit load times of the website and datasets, an offline parser was written in C# to delete unimportant data and bring the CSV files into a format the website would understand.</p>
									</article>

									<article class="box">
										<h3>Timeline</h3>
										<img style="max-width: 100%;" src="images/timeline-visualisation.png" alt="" />
										<p>To show any relation between the recent events and the sentiment and emotionality of tweets in the different languages, we chose to also add an interactive timeline to our website. This timeline includes the twitter data collected during the project as well as specially collected events data. Combined with the wordcloud for every language for any point in time, the timeline gives the visitor a nice overview over the development of the refugee crisis in europe.</p>
									</article>
									Have a look at our <a href="https://bitbucket.org/jneijt_stud/fhmw_cose8">repository</a> containing all our tools.
								</section>
								<!--<section class="box">
									<header>
										<h2>Datasets</h2>
									</header>
									<ul class="style3">
										<li><a href="#">Twitter_merged</a></li>
										<li><a href="#">Wikipedia_merged</a></li>
										<li><a href="#">Web_merged</a></li>
										<li><a href="#">Sed hendrerit massa nam mattis</a></li>
										<li><a href="#">Turpis vel leo accumsan aliquet</a></li>
									</ul>
								</section>-->
							</div>
						</div>
						<div class="8u 12u(mobile) important(mobile)">

							<!-- Content -->
							<div id="content">
								<article class="box post">
									<header class="style1">
										<h2>Methods and tools</h2>
									</header>
									<header class="style2">
										<h2>Method</h2>
									</header>
									<p>To find an answer to our question of how the sentiment about the refugee crisis in the european countries differs, we had to firstly collect as much data as possible
									for further analysis. We chose to focus on the basic sources for this project, namely Twitter, Wikipedia and Google. For each of these we will try to describe our
									methods a bit further down. But let us first have a look at what we wanted to achieve for all of the sources.</p>
									<img src="images/condor.png" alt="Condor software" style="width: 350px; float: right;" />
									<p>Using the network analysis software "Condor", we were able to fetch and analyse all of the sources chosen. It offers quick and easy acces to the main tools for
									analysis we were interested in:</p>
									<ul class="fa-ul" style="margin-left:0;">
										<li><i class="fa fa-arrow-circle-o-right"></i> Mentioned words/phrases</li>
										<li><i class="fa fa-arrow-circle-o-right"></i> Sentiment</li>
										<li><i class="fa fa-arrow-circle-o-right"></i> Network key indices (key actors, network connections, activity)</li>
									</ul>
									<p>Using these figures, we could then for example extract the key actors in each of the networks based on for instance their follower count in Twitter, or their
									calculated betweenness centrality in the network itself. The key actors themselves could be analysed further to find out more about the position of that actor in
									the network. Is it a political party tweeting about recent events, is it a newspaper, just some "normal" people?</p>

									<header class="style2">
										<h2>Sources and Keywords</h2>
									</header>
									<p>Twitter, Google and Wikipedia were chosen because they give a good basis for further analysis and because they all have a relatively accessible API which can
									be used. Facebook would have been very interesting as well, mainly because we think it is more widely spread in europe than Twitter, but as Facebook does not
									provide any usable API for these kinds of usage scenarios anymore, we had to discard that idea.</p>
									<p>Comment sections of newspapers for example would also have been interesting. And there are a couple of more interesting sources that we checked but none of
									them had an open API available, nor were the websites in a way that they could have been automatically scraped using a custom tool. Therefore, we had to stick to
									Twitter, Wikipedia and Google in the long run to get some data soon and not to spend too much time on developing custom fetchers.</p>
									<p>To make sure, we would fetch for the right keywords, we first defined the languages we were interested in. The languages available were limited as the
										Condor software does not support all languages and as we had to have at least someone in our team who could read the language in case we needed to
										further analyse specific data like the key actors. We then collected different keyword ideas for each language and cross referenced these keywords
										with the results from Google Trends to see which ones would be the most active. The top keyword was then chosen per language for all the subsequent
										fetches. These keywords are:</p>
									<ul class="fa-ul" style="margin-left:0;">
										<li><i class="fa fa-arrow-circle-o-right"></i> English: "Refugees"</li>
										<li><i class="fa fa-arrow-circle-o-right"></i> German: "Flüchtlinge"</li>
										<li><i class="fa fa-arrow-circle-o-right"></i> French: "Réfugiés"</li>
										<li><i class="fa fa-arrow-circle-o-right"></i> Italian: "Asilo"</li>
										<li><i class="fa fa-arrow-circle-o-right"></i> Spanish: "Asilo"</li>
									</ul>
									<header class="style2">
										<h2>Data size</h2>
									</header>
									<table>
										<thead>
											<tr>
												<td></td>
												<td><b>Web</b></td>
												<td><b>Twitter</b></td>
												<td><b>Wikipedia</b></td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><b>English</b></td>
												<td>473 actors</td>
												<td>42322 actors</td>
												<td>391 actors</td>
											</tr>
											<tr>
												<td><b>German</b></td>
												<td>1349 actors</td>
												<td>18046 actors</td>
												<td>47 actors</td>
											</tr>
											<tr>

												<td><b>French</b></td>
												<td>787 actors</td>
												<td>28559 actors</td>
												<td>not available</td>
											</tr>
											<tr>

												<td><b>Italian</b></td>
												<td>374 actors</td>
												<td>10340 actors</td>
												<td>6 actors</td>
											</tr>
											<tr>
												<td><b>Spanish</b></td>
												<td>390 actors</td>
												<td>25368 actors</td>
												<td>21 actors</td>
											</tr>
										</tbody>
									</table>
									<p>While we fetched the web, we were able to perform separate fetches for Germany, Austria and Switzerland, the German data set is the largest of all five sets.</p>
                                    <p>We restricted our twitter fetches to the 5 given languages. We tried out to restrict our twitter fetches to a certain geolocation which unfortunately did not lead to a satisfying result. Fetches in english provided the biggest amount of actors.</p>
									<p>The Wikipedia fetches were all restricted to the time between 01 May 2015 and 12 June 2016.</p>
									<header class="style2">
										<h2>Advantages & Disadvantages</h2>
									</header>
									<p><b>Web: Advantages</b><br>
									By fetching the web through Google's CSE API, we are able to limit our search results to certain languages and even geographical locations. For example, we can distinguish between German results from Germany, Austria, and Switzerland.</p>
									<p><b>Web: Disadvantages</b><br>
									Due to limitations of the Google CSE API's free tier, the amount of data collected is rather small compared to our Twitter results. Theoretically, the CSE API should return 100 results per day. In reality, we were only able to collect up to 50 results per day most of the time, in rare cases even less. In addition to these limitations, some of the pages in the result sets didn't seem to correspond to the fetch parameters provided to the fetch. This was especially the case for the language restrictions. For example, despite limiting the results to German, the result set for Germany included some pages in English and even Arabic.</p>

									<p><b>Twitter: Advantages</b><br>
									By fetching twitter through Twitter API, we were able to limit our search results to certain languages. The twitter API offers a lot of information like followers count, language, and geolocation. Thanks to these attributes it was easy to find the biggest influencers.</p>
									<p><b>Twitter: Disadvantages</b><br>
									Twitter fetches are limited by maximum number of tweets and a time of around 8 days. If there is a high volume of tweets in short time a automatic fetcher would be advantage to reduce the effort of fetching. There are two initial buckets available for GET requests: 15 calls every 15 minutes, and 180 calls every 15 minutes, which was a disadvantage in the beginning of the project while we were trying to find out the maximum number of fetchable tweets by try and error in condor.
Another disadvantage was the malfunctioning geolocation restriction, which did not allow us to restrict English tweets to english speaking european countries.</p>

									<p><b>Wikipedia: Advantages</b><br>
										The main advantage of Wikipedia is its ability to provide data going far back in time as the API has no limit how much of the history can be fetched. So theoretically one could fetch the history of one article up to its first creation on wikipedia.
									</p>
									<p><b>Wikipedia: Disadvantages</b><br>
										One Problem using Wikipedia was limiting our results to articles concerning the current migrant crisis as there is a lot of information available on Wikipedia as well about many other related topics like refugees in the Second World War or migration in general. This made it rather difficult to chose what articles to include in the final network and which articles to remove.<br>
										Another disadvantage was that the Condor software did not support fetching the Wikipedia Evolution network for the french language. So Wikipedia is actually the only source we were only able to collect 4 out of 5 languages.
									</p>
								</article>
								<!--
								<div class="row 150%">
									<div class="6u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Magna pulvinar tempus</h2>
											</header>
											<a href="#" class="image featured"><img src="images/pic05.jpg" alt="" /></a>
											<p>Rutrum bibendum. Proin pellentesque diam non ligula commodo tempor. Vivamus
											eget urna nibh. Curabitur non fringilla nisl. Donec accumsan interdum nisi, quis
											tempus.</p>
											<a href="#" class="button style1">More</a>
										</section>
									</div>
									<div class="6u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Magna pulvinar tempus</h2>
											</header>
											<a href="#" class="image featured"><img src="images/pic06.jpg" alt="" /></a>
											<p>Rutrum bibendum. Proin pellentesque diam non ligula commodo tempor. Vivamus
											eget urna nibh. Curabitur non fringilla nisl. Donec accumsan interdum nisi, quis
											tempus.</p>
											<a href="#" class="button style1">More</a>
										</section>
									</div>
								</div>
								-->
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Highlights -->
			<div class="wrapper style3">
				<div class="title" style="line-height: 2em;">Obstacles encountered<br>during the project</div>
				<div id="highlights" class="container">
					<div class="row 150%">
						<div class="6u 12u(mobile)">
							<section class="highlight">
								<img src="images/languageOverlap.jpg" alt="" />
								<h3>Language overlaps</h3>
								<p>Originally we were hoping to fetch data by country, but as the project advanced it became clear we would only be able to fetch by language. Of course this introduced one of our main obstacles during the project: Almost no language is spoken in only one country. The extremes of course being english and spanish which are spoken all over the world, but also less extreme cases like german which is spoken in Germany, Switzerland and Austria.</p>
								<p>And with many languages being spoken in one country as well, this introduced a kind of overlapping data for every country and language within europe and between european and non-european countries.</p>
							</section>
						</div>
						<div class="6u 12u(mobile)">
							<section class="highlight">
								<img src="images/localMarksTweets.jpg" alt="" />
								<h3>Lack of local marks on tweets</h3>
								<p>Unfortunately, the sources used, like twitter, do not always offer a reliable way of locating the data geographically. For instance, any twitter user may set his location to any text he likes, resulting in location information like "on a rock in the atlantic" or "planet earth" which obviously is not very precise. Lacking these reliable sources for information, we were unable to restrict our data to the european continent, let alone to specific countries.</p>
								<p>In the analysis it was therefore rather difficult to be able to definitively answer our main question with regard to countries and had to widen the question a bit to include languages as a whole, making assumptions for the differences in single countries.</p>
							</section>
						</div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<div id="footer-wrapper" class="wrapper" style="padding-top: 0;">
				<?php include ("footer.php"); ?>
			</div>
		</div>

		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/skel.min.js"></script>
		<script src="assets/js/skel-viewport.min.js"></script>
		<script src="assets/js/util.js"></script>
		<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
		<script src="assets/js/main.js"></script>
	</body>
</html>