<!DOCTYPE HTML>
<!--
	Escape Velocity by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Timeline of events :: COINS project team 8 - coolhunting about "asylum seekers"</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<style>
			.timeline-wrapper {
				width: 100%;
				height: 500px;
			}

			#timeline-info {
				text-align: center;
			}
			#timeline-info header.style1 {
				padding: 0;
			}
			#timeline-wordcloud {
				width: 100%;
				height: 600px;
				background: #f3f3f3;
			}

			#timeline {
				width: 100%;
				height: 100%;
                background: #f3f3f3;
			}
            #timeline-controls {
                text-align: center;
            }
            table {
                width: auto;
                margin: 0 auto;
            }
            table thead th {
                font-weight: bold;
                text-align: center;
            }
            table tbody th {
                font-weight: bold;
                text-align: right;
                width: 120px;
            }
            table td {
                text-align: center;
                width: 100px;
            }

			svg {
				font-size: 10px;
			}

            rect.bg-margin {
                fill: #f3f3f3;
            }

            rect.legend {
                fill: #f3f3f3;
                stroke; #ddd;
            }

			g {
				overflow: visible;
			}

			text {
				z-index: 20;
			}

            .graph-data {
                overflow: hidden;
            }

			rect.pane {
				cursor: move;
				fill: none;
				pointer-events: all;
			}
            
            path.line {
                fill: none;
                stroke-width: 1.5px;
            }

            path.area {
                opacity: 0.2;
            }

			circle.event {
				fill: #aaa;
				stroke: #fff;
				cursor: pointer;
			}

            .axis .tick {
                font-size: 14px;
            }

			.axis.y line,
			.axis.x line {
				fill: none;
				stroke: #ddd;
			}

            .axis.y path,
            .axis.x path {
                fill: none;
                stroke: #ddd;
                opacity: 0.1;
            }

			.x.axis .minor {
				stroke-opacity: .5;
			}
		</style>
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">
			<!-- Header -->
				<div id="header-wrapper" class="wrapper">
					<div id="header">

						<!-- Nav -->
							<?php $selected="timeline"; include ("nav.php"); ?>

					</div>
				</div>

			<!-- Main -->
				<div class="wrapper style2">
					<div class="title">What happened?</div>

					<div id="main" class="container">

						<!-- Content -->
							<div id="content">
								<article class="box post">
									<header class="style1">
										<h2>Explore some of the most important events<br class="mobile-hide" />
										that happened in Europe</h2>
										<p>Click on one of the events to get more information about it and the sentiment in Europe at that time, dotted lines represent interpolated data where no fetches are available.</p>
									</header>
                                    <div id="timeline-controls">
                                        <table>
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>German</th>
                                                <th>English</th>
                                                <th>French</th>
                                                <th>Italian</th>
                                                <th>Spanish</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th>Sentiment</th>
                                                <td><input type="checkbox" name="timeline-data" value="sentiment-de" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="sentiment-en" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="sentiment-fr" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="sentiment-it" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="sentiment-es" checked="checked"/></td>
                                            </tr>
                                            <tr>
                                                <th>Emotionality</th>
                                                <td><input type="checkbox" name="timeline-data" value="emotionality-de" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="emotionality-en" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="emotionality-fr" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="emotionality-it" checked="checked"/></td>
                                                <td><input type="checkbox" name="timeline-data" value="emotionality-es" checked="checked"/></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <input type="checkbox" name="timeline-showAreas" value="showAreas" checked="checked"/> Fill areas below line
                                    </div>
                                    <br>
									<div class="timeline-wrapper">
                                        <div id="timeline">
										</div>
									</div>
                                    <br>
                                    <div id="timeline-info">
                                        <div class="box">
                                            <header class="style1"><h2 class="timeline-info-title"></h2></header>
                                            <div class="timeline-info-description"></div>
                                        </div>
										<br>
                                        <div style="text-align: center">
											<h3>Wordcloud showing the sentiment up to the selected date:</h3>
											<input type="radio" name="wordcloud-data" value="de" checked="checked" /> German
											<input type="radio" name="wordcloud-data" value="en" /> English
											<input type="radio" name="wordcloud-data" value="fr" /> French
											<input type="radio" name="wordcloud-data" value="it" /> Italian
											<input type="radio" name="wordcloud-data" value="es" /> Spanish
											<br><br>
                                            <div id="timeline-wordcloud"></div>
                                        </div>
                                    </div>
                                </article>

								<!--
								<div class="row 150%">
									<div class="4u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Magna pulvinar tempus</h2>
											</header>
											<a href="#" class="image featured"><img src="images/pic05.jpg" alt="" /></a>
											<p>Rutrum bibendum. Proin pellentesque diam non ligula commodo tempor. Vivamus
											eget urna nibh. Curabitur non fringilla nisl. Donec accumsan interdum nisi, quis
											tempus.</p>
											<a href="#" class="button style1">More</a>
										</section>
									</div>
									<div class="4u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Magna pulvinar tempus</h2>
											</header>
											<a href="#" class="image featured"><img src="images/pic06.jpg" alt="" /></a>
											<p>Rutrum bibendum. Proin pellentesque diam non ligula commodo tempor. Vivamus
											eget urna nibh. Curabitur non fringilla nisl. Donec accumsan interdum nisi, quis
											tempus.</p>
											<a href="#" class="button style1">More</a>
										</section>
									</div>
									<div class="4u 12u(mobile)">
										<section class="box">
											<header>
												<h2>Magna pulvinar tempus</h2>
											</header>
											<a href="#" class="image featured"><img src="images/pic07.jpg" alt="" /></a>
											<p>Rutrum bibendum. Proin pellentesque diam non ligula commodo tempor. Vivamus
											eget urna nibh. Curabitur non fringilla nisl. Donec accumsan interdum nisi, quis
											tempus.</p>
											<a href="#" class="button style1">More</a>
										</section>
									</div>
								</div>
								-->
							</div>

					</div>
				</div>


			<!-- Highlights -->
			<!--
				<div class="wrapper style3">
					<div class="title">The Endorsements</div>
					<div id="highlights" class="container">
						<div class="row 150%">
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
									<h3><a href="#">Aliquam diam consequat</a></h3>
									<p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
									<ul class="actions">
										<li><a href="#" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
									<h3><a href="#">Nisl adipiscing sed lorem</a></h3>
									<p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
									<ul class="actions">
										<li><a href="#" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
							<div class="4u 12u(mobile)">
								<section class="highlight">
									<a href="#" class="image featured"><img src="images/pic04.jpg" alt="" /></a>
									<h3><a href="#">Mattis tempus lorem</a></h3>
									<p>Eget mattis at, laoreet vel amet sed velit aliquam diam ante, dolor aliquet sit amet vulputate mattis amet laoreet lorem.</p>
									<ul class="actions">
										<li><a href="#" class="button style1">Learn More</a></li>
									</ul>
								</section>
							</div>
						</div>
					</div>
				</div>
				-->

			<!-- Footer -->
				<div id="footer-wrapper" class="wrapper">

					<?php include ("footer.php"); ?>

				</div>

		</div>

		<!-- Scripts -->

			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			<script src="assets/js/d3/d3.min.js"></script>
			<script src="assets/js/d3/d3.layout.cloud.js"></script>
			<script src="assets/js/wordcloud.js"></script>
			<script src="assets/js/timeline.js"></script>

	</body>
</html>