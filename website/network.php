<!DOCTYPE HTML>
<!--
	Escape Velocity by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Networks :: COINS project team 8 - coolhunting about "asylum seekers"</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
        <style>
            #graph, #graph-container {
                width: 100%;
                height: 700px;
                background: #f3f3f3;
				position: relative;
            }

            #graph .initNote {
                width: 100%;
                text-align: center;
                font-size: 2em;
                font-weight: bold;
                padding: 200px 0 0 0;
				position: absolute;
				z-index: 1000;
            }
        </style>

        <!-- START SIGMA IMPORTS -->

		<script src="assets/js/linkurious/dist/sigma.min.js"></script>
		<script src="assets/js/linkurious/dist/plugins.min.js"></script>
        <!-- END SIGMA IMPORTS -->
	</head>
	<body class="right-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
			<div id="header-wrapper" class="wrapper">
				<div id="header">

					<!-- Nav -->
					<?php $selected="network"; include ("nav.php"); ?>
				</div>
			</div>

			<!-- Main -->
			<div class="wrapper style2">
				<div class="title">The networks</div>
				<div id="main" class="container">
					<div class="row 150%">
						<div class="8u 12u(mobile)">

							<!-- Content -->
							<div id="content">
								<article class="box post">
									<header class="style1">
										<h2>Explore the networks!</h2>
										<p>Use the filter possibilities on the right hand side.</p>
									</header>
									<div id="graph">
										<div class="initNote">
											<div class="message">Please select a dataset on the right.</div>
											<div class="progress"></div>
										</div>
										<div id="graph-container"></div>
									</div>
									<br>
									<p>You can explore the network connections of actors participating in the debate on Twitter and in blogs about the so-called refugee crisis.
										It is possible to look at languages separately or investigate interactions of them. In any setting the network displays the at most 6000
										most important actors (measured by degree centrality) which explains the different minimum degree for different settings. Limiting the total
										number of displayed actors is necessary to ensure performant displaying of the network in your browser.</p>
								</article>
							</div>
						</div>
						<div class="4u 12u(mobile)">

							<!-- Sidebar -->
							<div id="sidebar">
								<section class="box">
									<header>
										<h2>What are you looking for?</h2>
									</header>
									<p>You may choose a network and filter for the parameters you are interested in. </p>
								</section>
								<section class="box">
									<h2>Select dataset</h2>
									<form id="formDataset">
										<h3>Source</h3>
										<input type="radio" name="network-source" value="twitter" checked="checked"> Twitter<br>
										<input type="radio" name="network-source" value="web"> Web (Google)<br>
										<h3>Fetched language</h3>
										<input type="radio" name="network-language" value="en" checked="checked"> English (Keyword "Refugees")<br>
										<input type="radio" name="network-language" value="de"> German (Keyword "Flüchtlinge")<br>
										<input type="radio" name="network-language" value="fr"> French (Keyword "Réfugiés")<br>
										<input type="radio" name="network-language" value="it"> Italian (Keyword "Asilo")<br>
										<input type="radio" name="network-language" value="es"> Spanish (Keyword "Asilo")<br>
										<br>
										<a href="javascript:void(0);" id="btnDownloadDataset" class="button style1">Download</a>
									</form>

									<hr/>

									<h2>Size by</h2>
									<form id="formSize">
										<input type="radio" name="network-size" value="betweenness" checked="checked"> Betweenness centrality<br>
										<span class="no-web"><input type="radio" name="network-size" value="followers"> Followers count<br></span>
										<input type="radio" name="network-size" value="sentiment"> Average sentiment<br>
									</form>

									<hr/>

									<h2>Color by</h2>
									<form id="formColor">
										<input type="radio" name="network-color" value="betweenness"> Betweenness centrality<br>
										<span class="no-web"><input type="radio" name="network-color" value="followers"> Followers count<br></span>
										<input type="radio" name="network-color" value="sentiment" checked="checked"> Average sentiment<br>
									</form>
								</section>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<div id="footer-wrapper" class="wrapper">
				<?php include ("footer.php"); ?>
			</div>
		</div>

		<!-- Scripts -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery.dropotron.min.js"></script>
		<script src="assets/js/skel.min.js"></script>
		<script src="assets/js/skel-viewport.min.js"></script>
		<script src="assets/js/util.js"></script>
		<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
		<script src="assets/js/main.js"></script>
		<!--<script src="assets/js/cytoscape.min.js"></script> -->
        <script src="assets/js/linkurious/plugins/sigma.layouts.forceAtlas2/worker.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.layouts.forceAtlas2/supervisor.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.layouts.forceLink/worker.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.layouts.forceLink/supervisor.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.plugins.filter/sigma.plugins.filter.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.plugins.animate/sigma.plugins.animate.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.plugins.design/sigma.plugins.design.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.plugins.legend/sigma.plugins.legend.js"></script>
        <script src="assets/js/linkurious/plugins/sigma.plugins.legend/settings.js"></script>
        <script src="assets/js/network.js"></script>
	</body>
</html>