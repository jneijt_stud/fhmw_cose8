﻿using System;

namespace WordcloudParser.Models
{
    public class WordOccurence
    {
        public string Word { get; set; }
        public Double Sentiment { get; set; }
        public DateTime Time { get; set; }
        public int WordCount { get; set; }

        public WordOccurence()
        {
            WordCount = 1;
        }
    }
}