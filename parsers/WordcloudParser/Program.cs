﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using LINQtoCSV;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using WordcloudParser.Models;
using DataRow = LINQtoCSV.DataRow;

namespace WordcloudParser
{
    class Program
    {
        private readonly List<WordOccurence> _allWords;
        private readonly CsvFileDescription _csvDescription;
        private readonly Dictionary<DateTime, Dictionary<string, WordOccurence>> _wordsByDate;

        static void Main(string[] args)
        {
            var prog = new Program();
            Console.ReadLine();
        }

        public Program()
        {
            _allWords = new List<WordOccurence>();
            _wordsByDate = new Dictionary<DateTime, Dictionary<string, WordOccurence>>();
                _csvDescription = new CsvFileDescription()
            {
                SeparatorChar = ',',
                FirstLineHasColumnNames = false
            };

            ReadCsv(Settings.Default.Filename);
            Process();
            Export();
            Trace.TraceInformation("Finished task. Press any key to close.");
        }

        private void ReadCsv(string filename)
        {
            Trace.TraceInformation("Reading CSV file...");
            CsvContext cc = new CsvContext();
            IEnumerable<DataRow> enumerable = cc.Read<DataRow>(filename, _csvDescription);
            var enumerator = enumerable.GetEnumerator();
            if (enumerator.MoveNext())
            {
                int sentimentColumn = FindSentimentColumn(enumerator.Current);
                int datetimeColumn = FindDateTimeColumn(enumerator.Current);

                if (sentimentColumn < 0 || datetimeColumn < 0)
                {
                    return;
                }

                while (enumerator.MoveNext())
                {
                    var value = enumerator.Current[sentimentColumn].Value;
                    if (!String.IsNullOrEmpty(value))
                    {
                        value = value.Replace("{", "");
                        value = value.Replace("}", "");
                        var words = value.Split(',');

                        foreach (var w in words)
                        {
                            var parts = w.Split('=');
                            if (parts.Length == 2)
                            {
                                var word = new WordOccurence()
                                {
                                    Word = Regex.Replace(parts[0].Trim(), @"\r\n?|\n", ""),
                                    Sentiment = Double.Parse(parts[1]),
                                    Time = DateTime.Parse(enumerator.Current[datetimeColumn].Value)
                                };
                                _allWords.Add(word);
                            }
                        }
                    }
                }
            }
        }

        private void Process()
        {
            Trace.TraceInformation("Processing data...");
            var allWordsByDate = new Dictionary<DateTime, Dictionary<string, WordOccurence>>();
            foreach (var occurrence in _allWords)
            {
                var date = occurrence.Time.Date;
                if (!allWordsByDate.ContainsKey(date))
                {
                    allWordsByDate.Add(date, new Dictionary<string, WordOccurence>());
                }

                if (allWordsByDate[date].ContainsKey(occurrence.Word))
                {
                    allWordsByDate[date][occurrence.Word].Sentiment += occurrence.Sentiment;
                    allWordsByDate[date][occurrence.Word].WordCount++;
                }
                else
                {
                    allWordsByDate[date].Add(occurrence.Word, occurrence);
                }
            }
            foreach (var date in allWordsByDate)
            {
                _wordsByDate.Add(date.Key, new Dictionary<string, WordOccurence>());

                foreach (var word in date.Value)
                {
                    word.Value.Sentiment = word.Value.Sentiment/word.Value.WordCount;
                }
                
                var ordered = date.Value.OrderByDescending(key => key.Value.WordCount).Take(Settings.Default.SelectTopAmount);
                foreach (var word in ordered)
                {
                    _wordsByDate[date.Key].Add(word.Key, word.Value);
                }
            }
        }

        private void Export()
        {
            foreach (var date in _wordsByDate)
            {
                Trace.TraceInformation("Words for {0}", date.Key.Date);
                foreach (var word in date.Value)
                {
                    Trace.TraceInformation("\t{0}\t= {1}", word.Value.Word, word.Value.Sentiment);
                }
            }
            var export = JsonConvert.SerializeObject(_wordsByDate.Select(elem => new { Date = elem.Key, Words = elem.Value.Values.ToArray() }));
            System.IO.File.WriteAllText(Settings.Default.OutputFilename, export);
        }

        private int FindSentimentColumn(DataRow row)
        {
            for (var i = 0; i < row.Count; i++)
            {
                if (row[i].Value == Settings.Default.SentimentColumnHeader)
                {
                    return i;
                }
            }
            Trace.TraceError("Could not find column with specified column name.");
            return -1;
        }

        private int FindDateTimeColumn(DataRow row)
        {
            for (var i = 0; i < row.Count; i++)
            {
                if (row[i].Value == Settings.Default.DateTimeColumnHeader)
                {
                    return i;
                }
            }
            Trace.TraceError("Could not find column with specified column name.");
            return -1;
        }
    }
}
