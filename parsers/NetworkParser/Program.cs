﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using LINQtoCSV;
using NetworkParser.Models;
using Newtonsoft.Json;

namespace NetworkParser
{
    class Program
    {
        private readonly string[] _sources = {"Twitter", "Web"};
        private readonly string[] _languages = {"de", "en", "it", "fr", "es"};
        private readonly CsvFileDescription _csvDescription;
        private Dictionary<string, Dictionary<string, LanguageDataSet>> _languageSets;

        static void Main(string[] args)
        {
            var prog = new Program();
            Console.ReadLine();
        }

        public Program()
        {
            _languageSets = new Dictionary<string, Dictionary<string, LanguageDataSet>>();

            _csvDescription = new CsvFileDescription()
            {
                SeparatorChar = ',',
                FirstLineHasColumnNames = true,
                IgnoreUnknownColumns = true
            };

            ReadAll();
            Process();
            Export();
            Trace.TraceInformation("Finished task. Press any key to close.");
        }

        private void ReadAll()
        {
            foreach (var source in _sources)
            {
                var set = new Dictionary<string, LanguageDataSet>();
                foreach (var lang in _languages)
                {
                    Trace.TraceInformation("Reading {1} CSV file for language {0}...", lang, source);

                    var nodes = ReadActors(source, lang);
                    var edges = ReadLinks(source, lang);

                    if (nodes != null && nodes.Any())
                    {
                        set.Add(lang, new LanguageDataSet(nodes.ToArray(), edges.ToArray()));
                        Trace.TraceInformation("\tRead {0} actors", nodes.Count());
                        Trace.TraceInformation("\tRead {0} edges", edges.Count());
                    }
                }
                _languageSets.Add(source, set);
            }
        }

        private void Process()
        {
            foreach (var source in _languageSets)
            {
                foreach (var languageSet in source.Value)
                {
                    var lastCount = languageSet.Value.Nodes.Length;
                    var nodeCount = languageSet.Value.Nodes.Length;
                    Trace.TraceInformation("Processing {0}", languageSet.Key);
                    Trace.TraceInformation("\tNode count: {0}", nodeCount);
                    Trace.TraceInformation("\tEdge count: {0}", languageSet.Value.Edges.Length);
                    languageSet.Value.Nodes = languageSet.Value.Nodes.GroupBy(a => a.UUID).Select(g => g.First()).ToArray();
                    Trace.TraceInformation("\tRemoved {0} duplicate nodes.", nodeCount - languageSet.Value.Nodes.Length);
                    while (lastCount > 6000)
                    {
                        languageSet.Value.MinDegree++;
                        Trace.TraceInformation("\tProcessing with new minimum degree: {0}...", languageSet.Value.MinDegree);
                        languageSet.Value.Nodes = languageSet.Value.Nodes.Where(a => a.DegreeCentrality >= languageSet.Value.MinDegree).ToArray();
                        var uuids = languageSet.Value.Nodes.Select(a => a.UUID).ToList();
                        languageSet.Value.Edges = languageSet.Value.Edges.Where(e => uuids.Contains(e.Source) && uuids.Contains(e.Target)).ToArray();
                        lastCount = languageSet.Value.Nodes.Length;
                    }
                    Trace.TraceInformation("\tFinal node count: {0}", languageSet.Value.Nodes.Length);
                    Trace.TraceInformation("\tFinal edge count: {0}", languageSet.Value.Edges.Length);
                }
            }
        }

        private void Export()
        {
            foreach (var source in _languageSets)
            {
                foreach (var languageSet in source.Value)
                {
                    var data = new
                    {
                        info = new
                        {
                            nodeCount = languageSet.Value.Nodes.Length,
                            edgeCount = languageSet.Value.Edges.Length,
                            minDegree = languageSet.Value.MinDegree
                        },
                        data = languageSet.Value
                    };
                    var export = JsonConvert.SerializeObject(data);
                    File.WriteAllText(
                        Path.Combine(Settings.Default.OutputDirectory, "network-" + source.Key.ToLower() + "-" + languageSet.Key + ".json"),
                        export);
                }
            }
        }

        private IEnumerable<Link> ReadLinks(string source, string language)
        {
            IEnumerable<Link> edges = null;
            var path = Path.Combine(Settings.Default.SourceDirectory, source + "Links_" + language + ".csv");
            if (File.Exists(path))
                edges = ReadFile<Link>(path);
            else
                Trace.TraceWarning("Could not read file for language {0}, file does not exist: \n{1}", language, path);
            return edges;
        }

        private IEnumerable<IActor> ReadActors(string source, string language)
        {
            Random r = new Random();
            var path = Path.Combine(Settings.Default.SourceDirectory, source + "Actors_" + language + ".csv");
            var maxSize = 1000;
            IEnumerable<IActor> nodes = null;
            if (File.Exists(path))
            {
                switch (source)
                {
                    case "Twitter":
                        nodes = ReadFile<TwitterActor>(path);
                        break;
                    case "Web":
                        nodes = ReadFile<WebActor>(path);
                        maxSize = 50;
                        break;
                }
                
                foreach (var actor in nodes)
                {
                    actor.PositionX = r.Next(0, maxSize);
                    actor.PositionY = r.Next(0, maxSize);
                }
            }
            else
                Trace.TraceWarning("Could not read file for language {0}, file does not exist: \n{1}", language, path);
            return nodes;
        }

        private T[] ReadFile<T>(string filename)
            where T : class, new() 
        {
            CsvContext cc = new CsvContext();
            var list = cc.Read<T>(filename, _csvDescription).ToArray();
            return list;
        }
    }
}
