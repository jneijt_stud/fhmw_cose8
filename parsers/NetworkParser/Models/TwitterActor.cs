﻿using System;
using LINQtoCSV;
using Newtonsoft.Json;

namespace NetworkParser.Models
{
    public class TwitterActor : IActor
    {
        [JsonIgnore]
        [CsvColumn(Name = "timezone")]
        public string Timezone { get; set; }

        [JsonIgnore]
        [CsvColumn(Name = "description")]
        public string Description { get; set; }

        [JsonIgnore]
        [CsvColumn(Name = "fullName")]
        public string FullName { get; set; }

        [JsonProperty(PropertyName = "sentiment")]
        [CsvColumn(Name = "avg sentiment")]
        public double AverageSentiment { get; set; }

        [JsonIgnore]
        [CsvColumn(Name = "language")]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "betweenness")]
        [CsvColumn(Name = "Betweenness centrality")]
        public double BetweennessCentrality { get; set; }

        [JsonProperty(PropertyName = "followers")]
        [CsvColumn(Name = "followers_count")]
        public int FollowersCount { get; set; }

        [JsonIgnore]
        [CsvColumn(Name = "Starttime")]
        public DateTime DateTime { get; set; }

        [JsonProperty(PropertyName = "degree")]
        [CsvColumn(Name = "Degree centrality")]
        public int DegreeCentrality { get; set; }

        [JsonProperty(PropertyName = "id")]
        [CsvColumn(Name = "Uuid")]
        public string UUID { get; set; }

        [JsonProperty(PropertyName = "x")]
        public int PositionX { set; get; }

        [JsonProperty(PropertyName = "y")]
        public int PositionY { set; get; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get { return UUID; } }

    }
}