﻿using System;

namespace NetworkParser.Models
{
    public interface IActor
    {
        int DegreeCentrality { get; set; }
        string UUID { get; set; }
        int PositionX { set; get; }
        int PositionY { set; get; }
    }
}