﻿using System;
using LINQtoCSV;
using Newtonsoft.Json;

namespace NetworkParser.Models
{
    public class WebActor : IActor
    {
        [JsonProperty(PropertyName = "sentiment")]
        [CsvColumn(Name = "sentiment")]
        public double Sentiment { get; set; }

        [JsonProperty(PropertyName = "betweenness")]
        [CsvColumn(Name = "Betweenness centrality")]
        public double BetweennessCentrality { get; set; }

        [JsonIgnore]
        [CsvColumn(Name = "Starttime")]
        public DateTime DateTime { get; set; }

        [JsonProperty(PropertyName = "degree")]
        [CsvColumn(Name = "Degree centrality")]
        public int DegreeCentrality { get; set; }

        [JsonProperty(PropertyName = "id")]
        [CsvColumn(Name = "Uuid")]
        public string UUID { get; set; }

        [JsonProperty(PropertyName = "color")]
        public string HexColor { get; set; } = "#000000";

        [JsonProperty(PropertyName = "x")]
        public int PositionX { set; get; }

        [JsonProperty(PropertyName = "y")]
        public int PositionY { set; get; }

        [JsonProperty(PropertyName = "label")]
        public string Label { get { return UUID; } }

    }
}