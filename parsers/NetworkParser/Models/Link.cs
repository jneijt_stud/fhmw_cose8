﻿using System.Xml.Serialization;
using LINQtoCSV;
using Newtonsoft.Json;

namespace NetworkParser.Models
{
    [XmlType("edge")]
    public class Link
    {
        [CsvColumn(Name = "Uuid")]
        [JsonProperty(PropertyName = "id")]
        [XmlAttribute(AttributeName = "id")]
        public string UUID { get; set; }

        [CsvColumn(Name = "TargetUuid")]
        [JsonProperty(PropertyName = "target")]
        [XmlAttribute(AttributeName = "target")]
        public string Target { get; set; }

        [CsvColumn(Name = "SourceUuid")]
        [JsonProperty(PropertyName = "source")]
        [XmlAttribute(AttributeName = "source")]
        public string Source { get; set; }
    }
}