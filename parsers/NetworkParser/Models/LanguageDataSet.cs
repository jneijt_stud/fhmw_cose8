﻿using Newtonsoft.Json;

namespace NetworkParser.Models
{
    public class LanguageDataSet
    {
        [JsonProperty(PropertyName = "nodes")]
        public IActor[] Nodes { get; set; }
        [JsonProperty(PropertyName = "edges")]
        public Link[] Edges { get; set; }
        [JsonIgnore]
        public int MinDegree { get; set; }

        public LanguageDataSet()
        {
            Nodes = new IActor[0];
            Edges = new Link[0];
        }

        public LanguageDataSet(IActor[] nodes, Link[] edges)
        {
            Nodes = nodes;
            Edges = edges;
        }
    }
}